package main

import (
	"fmt"
	"encoding/json"
	"log"
	"net/http"
	"io"
	
	"database/sql"

  _ "github.com/lib/pq"
)

const (
  host     = "db"
  port     = 5432
  user     = "postgres"
  password = "postgres"
  dbname   = "postgres"
)

type currency struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Usdrub string `json:"USDRUB"`
		Eurrub string `json:"EURRUB"`
	} `json:"data"`
}

func getrates() string { 
	var client http.Client
	resp, err := client.Get("http://currate.ru/api/?get=rates&pairs=USDRUB,EURRUB&key=b8c3c53ba9f744e70e3e9b51a4baeff1")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

    bodyBytes, err := io.ReadAll(resp.Body)
    if err != nil {
        log.Fatal(err)
    }
    bodyString := string(bodyBytes)
    return bodyString
}

func getcurrency() currency {
	var result = getrates()
		
	var cur currency
	err := json.Unmarshal([]byte(result), &cur)
	if err != nil {
		log.Fatalf("Error occured during unmarshaling. Error: %s", err.Error())
	}
	return cur
}

func isdbconnected() bool{
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return false
	}

	err = db.Ping()
	if err != nil {
		return false
	}

	return true;
}

func main() {
	
	var cur = getcurrency()
	fmt.Println("USD RUB = ", cur.Data.Usdrub)
	fmt.Println("EUR RUB = ", cur.Data.Eurrub)
	if isdbconnected() {
		fmt.Println("Database is successfully connected!")
	}
}