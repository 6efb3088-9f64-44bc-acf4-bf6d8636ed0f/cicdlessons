FROM golang:1.18

ENV PROJECT_DIR get-currency

ADD . /$PROJECT_DIR
WORKDIR /$PROJECT_DIR

RUN echo $PWD && ls -la
RUN go mod init getcurrency
RUN go get -u github.com/lib/pq
RUN go build

ENTRYPOINT ["./getcurrency"]
